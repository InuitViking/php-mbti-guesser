<?php

include __DIR__ . '/../vendor/autoload.php';

use Rubix\ML\PersistentModel;
use Rubix\Server\HTTPServer;
use Rubix\ML\Persisters\Filesystem;

$estimators = [];
$severs = [];

echo "Starting up INTJ server...";
$estimator = PersistentModel::load(new Filesystem('../INTJ_models/INTJ.rbx'));
$server = new HTTPServer('127.0.0.1', 8000);
$server->serve($estimator);