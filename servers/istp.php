<?php

include __DIR__ . '/../vendor/autoload.php';

use Rubix\ML\PersistentModel;
use Rubix\Server\HTTPServer;
use Rubix\ML\Persisters\Filesystem;

$estimators = [];
$severs = [];

echo "Starting up ISTP server...";
$estimator = PersistentModel::load(new Filesystem('../ISTP_models/ISTP.rbx'));
$server = new HTTPServer('127.0.0.1', 8003);
$server->serve($estimator);