<?php

include __DIR__ . '/../vendor/autoload.php';

use Rubix\ML\PersistentModel;
use Rubix\Server\HTTPServer;
use Rubix\ML\Persisters\Filesystem;

$estimators = [];
$severs = [];

echo "Starting up ISTJ server...";
$estimator = PersistentModel::load(new Filesystem('../ISTJ_models/ISTJ.rbx'));
$server = new HTTPServer('127.0.0.1', 8002);
$server->serve($estimator);