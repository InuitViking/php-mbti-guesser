<?php

include __DIR__ . '/../vendor/autoload.php';

use Rubix\ML\PersistentModel;
use Rubix\Server\HTTPServer;
use Rubix\ML\Persisters\Filesystem;

$estimators = [];
$severs = [];

echo "Starting up INTP server...";
$estimator = PersistentModel::load(new Filesystem('../INTP_models/INTP.rbx'));
$server = new HTTPServer('127.0.0.1', 8001);
$server->serve($estimator);