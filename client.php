<?php

include __DIR__ . '/vendor/autoload.php';

use Rubix\ML\Datasets\Unlabeled;
use Rubix\Server\RESTClient;

while (empty($text)) $text = readline("Enter some text to analyze:\n");

$models = [
	'intj',
	'intp',
	'istj',
	'istp',
];

$port = 8000;

foreach ($models as $model) {
	$client = new RESTClient('127.0.0.1', $port);
	$dataset = new Unlabeled([
		[$text],
	]);
	$predictions = $client->proba($dataset);
	echo $model . ': ' . ($predictions[0][1] * 100) . "%\n";
	$port++;
}