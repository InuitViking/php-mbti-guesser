<?php

include __DIR__ . '/vendor/autoload.php';

use Rubix\ML\PersistentModel;
use Rubix\Server\HTTPServer;
use Rubix\ML\Persisters\Filesystem;

$models = [
	'INTJ',
	'INTP',
	'ISTJ',
	'ISTP',
];

$estimators = [];
$severs = [];

echo "Starting up servers\n";
foreach ($models as &$model) {
	$model = strtolower($model);
	//"cd servers; php $model.php & exit"
	exec(sprintf("%s > %s 2>&1 & echo $! >> %s", "cd servers; php $model.php", 'server.log', 'server.pid'));
}
echo "Done!\n";